﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class moveStergenUp : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private GameObject[] ring = new GameObject[4];
    [SerializeField] private Collider[] ringCol = new Collider[4];
    [SerializeField] private Collider stergenC;
    [SerializeField] private float rotZ;
    private bool onRotate;
    public int startRot;
    [SerializeField] private golobolomka glb;
    [SerializeField] private ring ring1;
    [SerializeField] private ring ring2;
    [SerializeField] private ring ring3;
    [SerializeField] private ring ring4;

    void Awake()
    {

        ring1 = ring[0].GetComponent<ring>();
        ring2 = ring[1].GetComponent<ring>();
        ring3 = ring[2].GetComponent<ring>();
        ring4 = ring[3].GetComponent<ring>();

    }

    private void Update()
    {
        if (onRotate)
        {

            if (stergenC.isTrigger) glb.Key_Up();
            if (ringCol[0].isTrigger) ring1.Key_Up();
            if (ringCol[1].isTrigger) ring2.Key_Up();
            if (ringCol[2].isTrigger) ring3.Key_Up();
            if (ringCol[3].isTrigger) ring4.Key_Up();

            if (startRot > 0)
            {
                --startRot;
                onRotate = false;
                if (stergenC.isTrigger) stergenC.transform.Rotate(new Vector3(0f, 0f, -rotZ * Time.deltaTime));
                for (int i = 0; i < 4; i++)
                {
                    if (ringCol[i].isTrigger) ringCol[i].transform.Rotate(new Vector3(0f, 0f, -rotZ * Time.deltaTime));
                }
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        onRotate = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        onRotate = false;
    }
}
