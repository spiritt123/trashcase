﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class moveStergenLeft : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private Collider stergenC;
    [SerializeField] private float posZ;
    private bool onTranslate;
    public int startPos;
    [SerializeField] private golobolomka glb;

    private void Update()
    {
        if (onTranslate)
            glb.Key_Left();

        if (onTranslate && (startPos > 0))
        {
            --startPos;
            onTranslate = false;
            if (stergenC.isTrigger) stergenC.transform.Translate(new Vector3(0f, 0f, posZ * Time.deltaTime));
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        onTranslate = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        onTranslate = false;
    }
}
