using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;//onload
using System;//onload


public class golobolomka : MonoBehaviour {
    
    public int x,y,xs,ys,key=1;
    
    public GameObject[] ring = new GameObject[4];
    [SerializeField]private Material[] ringMaterial = new Material[4];
    
    [SerializeField] private GameObject go;
    [SerializeField] private moveStergenUp msUp;
    [SerializeField] private moveStergenDown msDown;
    [SerializeField] private moveStergenRight msRight;
    [SerializeField] private moveStergenLeft msLeft;

    [SerializeField] private Collider[] ringCol = new Collider[4];
    [SerializeField] private Collider ringColSt;

    
    int[,] map;
    public Mazes ms;

    private void Start () {

        ms = new Mazes();
        
        Init();
    }

    private void Init(){
        string aaa = "";
        map = ms.Init(5);
        for (int i = 0; i < map.GetLength(0); i++)
        {
            for (int j = 0; j < map.GetLength(1); j++)
            {
                aaa += map[i,j];
            }
            print(aaa);
            aaa = "";
        }
        //x = map.Length / 44 - 1;
        //for(int i=0; i<44; ++i){
        //    if (map[i,x] == 0){
        //        y=i;
        //        break;
        //    }
        //}
        x = 1;
        y = 1;
        xs=x;
        ys=y;
    }

    public void Key_Up()
    {
        msUp.startRot = 0;
        
        y +=1;
        if (y>=21) {y=0;}
        if (map[y,x]==1) {
            y-=1;
            Light();
        }
        else {
            msUp.startRot = 1;
        }
    }

    public void Key_Down()
    {
        msDown.startRot = 0;
        y -=1;
        if (y<0) {y=21;}
        if (map[y,x]==1) {
            y+=1;
            Light();
        }
        else{
            msDown.startRot = 1;
        }
    }

    public void Key_Right()
    {
        msRight.startPos = 0;

        x += 1;

        if (x > map.Length / 22 - 1)
        {
            Light();
            x -= 1;
        }
        else if (map[y, x] == 1)
        {
            Light();
            x -= 1;
        }
        else
        {
            msRight.startPos = 1;
        }
    }

    public void Key_Left()
    {
        msLeft.startPos = 0;
        x -= 1;

        if (x == 0)
        {
            if ((ringMaterial[key].color.r > 230) && (ringMaterial[key].color.r < 256) &&
               (ringMaterial[key].color.g > 230) && (ringMaterial[key].color.g < 256) &&
               (ringMaterial[key].color.b > 230) && (ringMaterial[key].color.b < 256))
            {
                key += 1;
                Init();
            }
        }

        if (map[y, x] == 1)
        {
            Light();
            x += 1;
        }
        else
        {
            msLeft.startPos = 1;
        }
    }

    public void Light(){
        //start light
    }

    void Update () {
        
    }

    

}
