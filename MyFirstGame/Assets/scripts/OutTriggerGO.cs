﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutTriggerGO : MonoBehaviour
{

    [SerializeField] private Collider[] ringCol = new Collider[4];
    [SerializeField] private Collider ringColSt;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform != null)
                {
                    PrintName(hit.transform.gameObject);
                }
            }
        }
    }

    private void PrintName(GameObject go)
    {
        ringColSt.isTrigger = false;
        for (int i = 0; i < 4; i++)
        {
            ringCol[i].isTrigger = false;
        }
        switch (go.name)
        {
            case ("a"):
                ringColSt.isTrigger = true;
                break;
            case ("c1"):
                ringCol[0].isTrigger = true;
                break;
            case ("c2"):
                ringCol[1].isTrigger = true;
                break;
            case ("c3"):
                ringCol[2].isTrigger = true;
                break;
            case ("с4"):
                ringCol[3].isTrigger = true;
                break;
            default:
                break;
        }
    }
}
