using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;//onload
using System;//onload


public class ring : MonoBehaviour {

    //public Button Button_Up;
    //public Button Button_Down;

    [SerializeField] private moveStergenDown msDown;
    [SerializeField] private moveStergenUp msUp;

    public GameObject[] _ring = new GameObject[4];
    

    public rgb[] rc;
    
    
    public struct rgb{
    	public int r;
    	public int g;
    	public int b;

    	public void Rand_Init(System.Random rnd)
        {
            r = rnd.Next(0,255);
            g = rnd.Next(0,255);
    		b = rnd.Next(0,255);
    	}
    }

    public void Init(){

        System.Random rnd = new System.Random();
        rc = new rgb[4];
        for (int i=0; i<4; ++i){
			rc[i].Rand_Init(rnd);
    	}

    }

    void Start()
    {
        Init();
    }

    public void Key_Up()
    {
        for(int i=1; i<4; ++i){
			rc[i].r+=(rc[0].r)/10;
			rc[i].g+=(rc[0].g)/10;
			rc[i].b+=(rc[0].b)/10;
    	}
        msUp.startRot = 1;
    }

    public void Key_Down()
    {
    	for(int i=1; i<4; ++i){
			rc[i].r-=(rc[0].r)/10;
			rc[i].g-=(rc[0].g)/10;
			rc[i].b-=(rc[0].b)/10;
    	}
        msDown.startRot = 1;
    }

    void Update() {
        for (int i=1; i<4; ++i){
            _ring[i].GetComponent<Renderer>().material.color = new Color32((byte)rc[i].r, (byte)rc[i].g, (byte)rc[i].b, 200);
        }
	}

}
