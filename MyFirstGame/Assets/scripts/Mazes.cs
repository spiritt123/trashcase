
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;//onload
using System;//onload

using System.Collections;


public class Mazes : MonoBehaviour
{

    public struct dot
    {
        public bool visit;
        public bool l, d;
        public int i, j;
    }
    public System.Random rnd = new System.Random();
    public Stack<dot> steck = new Stack<dot>();
    public int line = 5;

    public dot[,] arr;

    public int[,] end_a;

    void Print_Map(dot[,] arr, int n, int k)
    {
        for (int i = 0; i < n * 2; ++i)
        {
            for (int j = 0; j < k * 2; ++j)
            {
                if (arr[i / 2, j / 2].l)
                {
                    end_a[i, j] = 1;
                }
                else { end_a[i, j] = 0; }
                ++j;
                end_a[i, j] = 0;
            }
            ++i;
            for (int j = 0; j < k * 2; ++j)
            {
                end_a[i, j] = 1;
                ++j;
                if (arr[i / 2, j / 2].d) { end_a[i, j] = 1; }
                else { end_a[i, j] = 0; }

            }
        }
    }

    bool Check_Down(dot a, dot[,] arr, int n, int k)
    {
        int x = a.j,
            y = a.i;
        if (y + 1 < n)
        {
            if (!arr[y + 1, x].visit) { return true; }
        }
        else
        {
            if (!arr[0, x].visit) { return true; }
        }
        return false;
    }

    bool Check_Up(dot a, dot[,] arr, int n, int k)
    {
        int x = a.j,
            y = a.i;
        if (y - 1 >= 0)
        {
            if (!arr[y - 1, x].visit) { return true; }
        }
        else
        {
            if (!arr[n - 1, x].visit) { return true; }
        }
        return false;
    }

    bool Check_Left(dot a, dot[,] arr, int n, int k)
    {
        int x = a.j,
            y = a.i;
        if (x - 1 >= 0)
        {
            if (!arr[y, x - 1].visit) { return true; }
        }
        return false;
    }

    bool Check_Right(dot a, dot[,] arr, int n, int k)
    {
        int x = a.j,
            y = a.i;
        if (x + 1 < k)
        {
            if (!arr[y, x + 1].visit) { return true; }
        }
        return false;
    }

    bool Check(dot a, dot[,] arr, int n, int k)
    {

        if (Check_Up(a, arr, n, k) ||
            Check_Right(a, arr, n, k) ||
            Check_Left(a, arr, n, k) ||
            Check_Down(a, arr, n, k))
        {
            return true;
        }

        return false;
    }

    public int[,] Init(int row)
    {
        end_a = new int[(line * 2), (row * 2)];
        arr = new dot[line, row];
        for (int i = 0; i < line; i++)
        {
            for (int j = 0; j < row; j++)
            {
                arr[i, j].i = i;
                arr[i, j].j = j;
                arr[i, j].l = true;
                arr[i, j].d = true;
                arr[i, j].visit = false;
            }
        }

        steck.Push(arr[2, 2]);

        while (steck.Count > 0)
        {
            if (Check(steck.Peek(), arr, line, row))
            {

                bool zero = true;
                while (zero)
                {
                    int a = rnd.Next(0, 4);
                    switch (a)
                    {
                        case 0:
                            if (Check_Up(steck.Peek(), arr, line, row))
                            {
                                arr[steck.Peek().i, steck.Peek().j].visit = true;
                                if (steck.Peek().i - 1 >= 0)
                                {
                                    steck.Push(arr[steck.Peek().i - 1, steck.Peek().j]);
                                }
                                else
                                {
                                    steck.Push(arr[line - 1, steck.Peek().j]);
                                }
                                arr[steck.Peek().i, steck.Peek().j].d = false;
                                zero = false;
                            }
                            break;
                        case 1:
                            if (Check_Right(steck.Peek(), arr, line, row))
                            {
                                arr[steck.Peek().i, steck.Peek().j].visit = true;
                                steck.Push(arr[steck.Peek().i, steck.Peek().j + 1]);
                                arr[steck.Peek().i, steck.Peek().j].l = false;
                                zero = false;
                            }
                            break;
                        case 2:
                            if (Check_Down(steck.Peek(), arr, line, row))
                            {
                                arr[steck.Peek().i, steck.Peek().j].visit = true;
                                arr[steck.Peek().i, steck.Peek().j].d = false;
                                if (steck.Peek().i + 1 == line)
                                {
                                    steck.Push(arr[0, steck.Peek().j]);
                                }
                                else
                                {
                                    steck.Push(arr[steck.Peek().i + 1, steck.Peek().j]);
                                }
                                zero = false;
                            }
                            break;
                        case 3:
                            if (Check_Left(steck.Peek(), arr, line, row))
                            {
                                arr[steck.Peek().i, steck.Peek().j].visit = true;
                                arr[steck.Peek().i, steck.Peek().j].l = false;
                                steck.Push(arr[steck.Peek().i, steck.Peek().j - 1]);
                                zero = false;
                            }
                            break;
                    }
                }

            }
            else
            {
                arr[steck.Peek().i, steck.Peek().j].visit = true;
                steck.Pop();
            }
        }

        Print_Map(arr, line, row);
        return end_a;
    }
}
