﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class ButtonZ : MonoBehaviour {

    public Button Button_Zoom;
    public Mouse ms;
    public GameObject gm;

    private Boolean see = false;

    void Start () {

        Button_Zoom.onClick.AddListener(Zoom);

    }

    private void Zoom()
    {
        if (ms.rotoldY < -45)
        {
            Debug.Log("bottom");
        }
        else if (ms.rotoldY > 45)
        {
            Debug.Log("top");
        }
        else
        {
            float mx = Math.Abs(ms.rotoldX);
            float mmx = Math.Abs(mx - 180);
            if (mx % 180 < 45 && mmx > 135)
            {
                gm.SetActive(see);
                if (!see)
                {
                    see = true;
                }
                else
                {
                    see = false;
                }
            }
            else if(mx > 135 && mmx <45)
            {
                Debug.Log("back");
            }
            else if(RotLeft())
            {
                Debug.Log("left");
            }else
            {
                Debug.Log("right");
            }
        }
    }

    private Boolean RotLeft()
    {
        if (ms.rotoldX>0)
        {
            if (ms.rotoldX - 180 > 45 && ms.rotoldX - 180 < 135)
            {
                return true;
            }
        }
        else
        {
            if (Math.Abs(ms.rotoldX) > 45 && Math.Abs(ms.rotoldX) < 135)
            {
                return true;
            }
        }
        return false;
    }

}


