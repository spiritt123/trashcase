﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Right : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Control c;
	
	

    public void OnPointerDown(PointerEventData eventData)
    {
        c.v = 5f;
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        c.v = 0f;
    }
}
