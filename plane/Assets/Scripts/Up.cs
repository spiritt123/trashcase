﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Up : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Control c;



    public void OnPointerDown(PointerEventData eventData)
    {
        c.l = 20.0f;
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        c.l = 0f;
    }
}
