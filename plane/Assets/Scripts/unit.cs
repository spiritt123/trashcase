﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class unit : MonoBehaviour {

    private Rigidbody2D rb;
    private Vector2 movement;
    public Control c;

    void Update()
    { 

        float inputX = Input.GetAxis("Horizontal")+1f;
        float inputY = Input.GetAxis("Vertical")+1f;  
        movement = new Vector2(c.v * inputX, c.l);
    }

    void FixedUpdate()
    {

        GetComponent<Rigidbody2D>().velocity = movement;
    }
}
